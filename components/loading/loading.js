import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";

class Loading extends Component {

  getHideClass(){

    var className = "hide";

    if (!this.props.loading){
      return className;
    } else {
      return "";
    }
  }

  render() {

    return (
      <div className={"loading "  + this.getHideClass()}>
        <img src="/images/circle.svg" />
      </div>
    )
  }
}

Loading.propTypes = {
  loading : PropTypes.bool
}

const mapStateToProps = ({ loading }) => {

  return {
    loading
  }
};

export default connect(mapStateToProps)(Loading);
