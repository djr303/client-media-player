import React, { Component } from "react";

class Results extends Component {

  getArtist(){

    return (
      <ul className="list artists">
        {this.props.files.artist.map((artist, index) => {
          return (<li key={index}>{artist}</li>);
        })}
      </ul>
    )
  }

  getAlbum(){

    return (
      <ul className="list albums">
        {this.props.files.album.map((album, index) => {
          return (<li key={index}>{album}</li>);
        })}
      </ul>
    )
  }

  getTracks(){

    return (
      <ul className="list tracks">
        {this.props.files.tracks.map((tracks, index) => {
          return (<li key={index}>{tracks.artist} _ {tracks.title}</li>)
        })}
      </ul>
    )
  }

  getVideos(){

    return (
      <ul className="list videos">
        {this.props.files.videos.map((video, index) => {
          return (<li key={index}>{video}</li>)
        })}
      </ul>
    )
  }

  render(){

    console.log("render()", this.props.displayAll);

    return(
      <div className="results">
        { !this.props.video && (this.props.textFilter.length > 0 || this.props.displayAll) ? (<div className="videos"><h1>_video</h1>{this.getVideos()}</div>) : null}
        { !this.props.artist && (this.props.textFilter.length > 0 || this.props.displayAll) ? (<div className="artists"><h1>_artists</h1>{this.getArtist()}</div>) : null }
        { !this.props.album && (this.props.textFilter.length > 0 || this.props.displayAll) ? (<div className="album"><h1>_albums</h1>{this.getAlbum()}</div>) : null}
        { !this.props.tracks && (this.props.textFilter.length > 0 || this.props.displayAll) ? (<div className="tracks"><h1>_tracks</h1>{this.getTracks()}</div>)  : null}
      </div>
    )
  }
}

export default Results;
