import React, { Component } from "react";

class SearchBar extends Component {

  onSubmit(e){
    e.preventDefault();

    this.props.onSubmit(this.input.value)
  }

  onScanClick(e){

    e.preventDefault();
    this.props.onScanClick();
  }

  componentDidMount(){
    this.input.focus()
  }

  render(){

    return(
      <div className="search">
        <div className="input">
          <form onSubmit={this.onSubmit.bind(this)}>
            <input placeholder="_search" ref={(node) => {this.input = node}} className="search-input" type="text" />
          </form>
        </div>
      </div>
    )
  }
}

export default SearchBar
