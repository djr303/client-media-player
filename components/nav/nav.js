import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { scanForFiles, toggleArtistFilter, toggleAlbumFilter, toggleTrackFilter, toggleVideoFilter, toggleDisplayAll } from "../../core/actions/actions";


class Nav extends Component {

  onScanClick(e){
    e.preventDefault();
    this.props.dispatch(scanForFiles());
  }

  onArtistClick(e){
    e.preventDefault();
    this.props.dispatch(toggleArtistFilter());
  }

  onAlbumClick(e){
    e.preventDefault();
    this.props.dispatch(toggleAlbumFilter());
  }

  onTracksClick(e){
    e.preventDefault();
    this.props.dispatch(toggleTrackFilter());
  }

  onVideoClick(e){
    e.preventDefault();
    this.props.dispatch(toggleVideoFilter());
  }

  onDisplayAllClick(e){
    e.preventDefault();
    this.props.dispatch(toggleDisplayAll());
  }

  getActiveClass(property){

    var className = "active";

    switch(property){
      case "artist":
        return this.props.filters.artist ? className : "";
      case "album":
        return this.props.filters.album ? className : "";
      case "tracks":
        return this.props.filters.tracks ? className : "";
      case "video":
        return this.props.filters.video ? className : "";
      case "displayAll":
        return this.props.filters.displayAll ? "" : className;
    }
  }

  render(){

    return(
      <nav>
        <div className="scan-btn">
          <a onClick={this.onScanClick.bind(this)}></a>
        </div>
        <div className={"list-all-btn " + this.getActiveClass("displayAll")}>
          <a onClick={this.onDisplayAllClick.bind(this)}></a>
        </div>
        <div className={"artist filter " + this.getActiveClass("artist")}>
          <a onClick={this.onArtistClick.bind(this)}>
            <span className="icon"></span>
            <span className="text">artist</span>
          </a>
        </div>
        <div className={"album filter " + this.getActiveClass("album")}>
          <a onClick={this.onAlbumClick.bind(this)}>
            <span className="icon"></span>
            <span className="text">album</span>
          </a>
        </div>
        <div className={"tracks filter " + this.getActiveClass("tracks")}>
          <a onClick={this.onTracksClick.bind(this)}>
            <span className="icon"></span>
            <span className="text">tracks</span>
          </a>
        </div>
        <div className={"video filter " + this.getActiveClass("video")}>
          <a onClick={this.onVideoClick.bind(this)}>
            <span className="icon"></span>
            <span className="text">video</span>
          </a>
        </div>
      </nav>
    )
  }
}

Nav.propTypes = {
  filters : PropTypes.object
}

const mapStateToProps = ({ filters }) => {
  return {
    filters
  }
};

export default connect(mapStateToProps)(Nav);
