import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";

class Messages extends Component {

  getHideClass(){

    var className = "hide";

    if (!this.props.messages || this.props.loading){
      return className;
    } else {
      return "";
    }
  }

  render() {

    return (
      <div className={"message "  + this.getHideClass()}>
        <h1>No files to search please hit re-scan_</h1>
      </div>
    )
  }
}

Messages.propTypes = {
  messages : PropTypes.bool
}

const mapStateToProps = ({ messages, loading }) => {

  return {
    messages,
    loading
  }
};

export default connect(mapStateToProps)(Messages);
