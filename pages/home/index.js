import React, { PropTypes } from 'react';
import { connect } from "react-redux";
import SearchBar from "../../components/searchBar/searchBar";
import Loading from "../../components/loading/loading";
import Messages from "../../components/messages/messages";
import Nav from "../../components/nav/nav";
import Results from "../../components/results/results";
import { scanForFiles, getFilesFromStorage, textFilter } from "../../core/actions/actions";
import resultsSelector from "../../core/selectors/results";

class HomePage extends React.Component {

  handleOnScanClick(){

    this.props.dispatch(scanForFiles());
  }

  componentWillMount(){

    this.props.dispatch(getFilesFromStorage());
  }

  handleOnSubmitClick(value){

    this.props.dispatch(textFilter(value))
  }

  render() {
    return (
      <div>
          <div className="container">
            <SearchBar onSubmit={this.handleOnSubmitClick.bind(this)} onScanClick={this.handleOnScanClick.bind(this)} />
            <Nav />
            <Results files={this.props.files}
                     displayAll={this.props.filters.displayAll}
                     textFilter={this.props.filters.textFilter}
                     artist={this.props.filters.artist}
                     album={this.props.filters.album}
                     tracks={this.props.filters.tracks}
                     video={this.props.filters.video} />
          </div>
          <Messages />
          <Loading />
      </div>
    );
  }
}

HomePage.propTypes = {
  files : PropTypes.object
}

const mapStateToProps = ({ files, videos, filters }) => {
  return {
    files : resultsSelector({filters, files}),
    videos : videos,
    filters
  }
};

export default connect(mapStateToProps)(HomePage);
