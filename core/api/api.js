export const scanForFiles = () => {

  var promise = new Promise((resolve, reject) => {

      function reqListener () {
          resolve(JSON.parse(this.responseText));
      }

      var oReq = new XMLHttpRequest();
      oReq.addEventListener("load", reqListener);
      oReq.open("GET", "http://localhost:3001/api/scan");
      oReq.timeout = 9393076;
      oReq.send();
  });

  return promise;

};

export const storeFileData = (payload) => {
  localStorage.files = JSON.stringify(payload);
}

export const getFilesFromStorage = () => {

  if (typeof localStorage.files === "undefined"){
    return null;
  } else {
    return JSON.parse(localStorage.files);
  }
};

