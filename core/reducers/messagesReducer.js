import * as types from '../constants';

// Handles video related actions
// The idea is to return an updated copy of the state depending on the action type.
export default function (state = false, action) {

  switch (action.type) {
    case types.NEEDS_FILE_SCAN:
      return true;
    case types.DOES_NOT_NEED_FILE_SCAN:
      return false;
    default:
      return state;
  }
}
