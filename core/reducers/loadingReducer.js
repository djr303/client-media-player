import * as types from '../constants';

// Handles video related actions
// The idea is to return an updated copy of the state depending on the action type.
export default function (state = false, action) {

  switch (action.type) {
    case types.LOADING_IN_PROGRESS:
      return true;
    case types.LOADING_NOT_IN_PROGRESS:
      return false;
    default:
      return state;
  }
}
