import * as types from '../constants';

export default function (state = {}, action) {

  switch (action.type) {
    case types.REQUEST_SCAN_FOR_FILES_SUCCESS:
      return Object.assign({}, action.files);
    case types.GET_FILES_FROM_STORAGE_SUCCESS:
      return Object.assign({}, action.files);
    default:
      return state;
  }
}
