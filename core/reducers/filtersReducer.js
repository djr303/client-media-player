import * as types from "../constants";

export default function (state = {textFilter: "", artist: false, album: false, tracks: false, video: false, displayAll:false}, action) {

  switch (action.type) {

    case types.FILTER_ARTIST:
      return Object.assign({}, state, {artist: !state.artist});
    case types.FILTER_ALBUM:
      return Object.assign({}, state, {album: !state.album});
    case types.FILTER_TRACKS:
      return Object.assign({}, state, {tracks: !state.tracks});
    case types.FILTER_VIDEO:
      return Object.assign({}, state, {video: !state.video});
    case types.TEXT_FILTER:
      return Object.assign({}, state, {textFilter: action.textFilter});
    case types.TOGGLE_DISPLAY_ALL:
      return Object.assign({}, state, { displayAll : !state.displayAll});
      break;
    default:
      return state;
  }
}
