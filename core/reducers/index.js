import { combineReducers } from "redux";
import files from "./filesReducer";
import loading from "./loadingReducer";
import messages from "./messagesReducer";
import filters from "./filtersReducer";

const rootReducer = combineReducers({
  files,
  loading,
  messages,
  filters
});

export default rootReducer;
