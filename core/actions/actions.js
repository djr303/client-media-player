import * as types from "../constants";

export const scanForFiles = () => ({
  type : types.REQUEST_SCAN_FOR_FILES
});

export const getFilesFromStorage = () => ({
  type: types.GET_FILES_FROM_STORAGE
})

export const toggleArtistFilter = () => ({
  type : types.FILTER_ARTIST
});

export const toggleAlbumFilter = () => ({
  type : types.FILTER_ALBUM
});

export const toggleTrackFilter = () => ({
  type : types.FILTER_TRACKS
});

export const toggleVideoFilter = () => ({
  type: types.FILTER_VIDEO
});

export const textFilter = (payload) => ({
  type : types.TEXT_FILTER,
  textFilter : payload
});

export const toggleDisplayAll = () => ({
  type : types.TOGGLE_DISPLAY_ALL
})
