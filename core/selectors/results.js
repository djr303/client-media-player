import { createSelector } from "reselect";

const getTextFilter = (state) => state.filters.textFilter;
const getDisplayAll = (state) => state.filters.displayAll;
const getFiles = (state) => state.files;

const arrContains = function(searchQuery, arr){

  var arrLen = arr.length;
  var ret = false;

  for (var i = 0; i < arrLen; i++){

    if (arr[i].toLowerCase().indexOf(searchQuery) > -1){
      ret = true;
      break;
    }
  }

  return ret;
}

const hasValidElement = function(textFilter, displayAll, element){

  if (displayAll && textFilter === ""){

    return true;
  } else {

    var searchQuery = textFilter.toLowerCase();
    var title = element.title ? element.title.toLowerCase() : false;
    var artist = element.artist && element.artist.length > 0 ? element.artist : false;
    var albumArtist = element.albumartist && element.albumartist.length > 0 ? element.albumartist : false;
    var album = element.album ? element.album.toLowerCase() : false;
    var genre = element.genre && element.genre.length > 0 ? element.genre : false;
    var filePath = element.filePath ? element.filePath.toLowerCase() : false;
    var ret = false;

    if ((title && title.indexOf(searchQuery) > -1)
      || (artist && arrContains(searchQuery, artist))
      || (albumArtist && arrContains(searchQuery, albumArtist))
      || (album && album.indexOf(searchQuery) > -1)
      || (genre && arrContains(searchQuery, genre))
      || (filePath && filePath.indexOf(searchQuery) > -1)) {
      ret = true;
    }

    return ret;
  }
}

const selector = createSelector(
  [getTextFilter, getDisplayAll, getFiles],
  (textFilter, displayAll, files) => {

    if (typeof files !== "undefined") {

      if (textFilter.length == 0 && !displayAll) {
        return {artist: [], album: [], tracks: [], videos: []};
      }

      var validElement;
      var artist;
      var albumArtist;
      var album;
      var arrLen = files.mp3s.length;
      var videoArrLen = files.videos.length;
      var artistArr = [];
      var albumArr = [];
      var tracksArr = [];
      var videos = [];
      var artistHash = {};
      var albumHash = {};
      var ret = {};

      for (var i = 0; i < arrLen; i++) {

        validElement = hasValidElement(textFilter, displayAll, files.mp3s[i]);

        artist = files.mp3s[i].artist;

        albumArtist = files.mp3s[i].albumartist;
        album = files.mp3s[i].album;

        if (artist && artist.length > 0 && validElement) {

          for (var j = 0; j < artist.length; j++) {
            if (!artistHash.hasOwnProperty(artist[j])) {
              artistHash[artist[j]] = true;
              artistArr.push(artist[j]);
            }
          }
        }

        if (albumArtist && albumArtist.length > 0 && validElement) {

          for (var j = 0; j < albumArtist.length; j++) {

            if (!artistHash.hasOwnProperty(albumArtist[j])) {
              artistHash[albumArtist[j]] = true;
              artistArr.push(albumArtist[j]);
            }
          }
        }

        if (album && validElement && !albumHash.hasOwnProperty(album)) {
          albumHash[album] = true;
          albumArr.push(album);
        }

        if (validElement) {
          tracksArr.push(files.mp3s[i])
        }
      }

      for (var i = 0; i < videoArrLen; i++) {

        if (files.videos[i].name.toLowerCase().indexOf(textFilter) > -1) {
          videos.push(files.videos[i].name);
        }
      }

      artistArr.sort();
      albumArr.sort();
      tracksArr.sort();
      videos.sort();

      ret.artist = artistArr;
      ret.album = albumArr;
      ret.tracks = tracksArr;
      ret.videos = videos;

      return ret;
    }

    return { artist : [], album : [], tracks : [], videos : []};

});

export default selector;

