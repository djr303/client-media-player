import { put, call } from "redux-saga/effects";
import { scanForFiles, storeFileData, getFilesFromStorage } from '../api/api';
import * as types from "../constants";

export function* filesSaga() {

    yield put({ type : types.LOADING_IN_PROGRESS});
    const files = yield call(scanForFiles, null);

    yield call(storeFileData, files);

    console.log("about to put files", Object.keys(files));

    //TODO: handle http 500 error
    yield [put({ type : types.LOADING_NOT_IN_PROGRESS}), put({ type : types.REQUEST_SCAN_FOR_FILES_SUCCESS, files}), put({ type : types.DOES_NOT_NEED_FILE_SCAN})];
}

export function* storageSaga(){

  yield put({ type: types.LOADING_IN_PROGRESS});

  const files = yield call(getFilesFromStorage, null);

  if (files == null){
    yield [put({ type: types.NEEDS_FILE_SCAN}), put({ type : types.LOADING_NOT_IN_PROGRESS})];
  } else{
    yield [put({ type : types.DOES_NOT_NEED_FILE_SCAN}), put({ type : types.GET_FILES_FROM_STORAGE_SUCCESS, files}), put({ type: types.LOADING_NOT_IN_PROGRESS})];
  }
}
