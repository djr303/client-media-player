import { fork } from 'redux-saga/effects';
import { watchScanForFiles, watchGetFilesFromStorage } from './watcher';

export default function* startForman() {
  yield [fork(watchScanForFiles), fork(watchGetFilesFromStorage)];
}
