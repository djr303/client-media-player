import { takeLatest } from 'redux-saga';
import { filesSaga, storageSaga } from './filesSaga';
import * as types from '../constants';

export function* watchScanForFiles(){

  yield* takeLatest(types.REQUEST_SCAN_FOR_FILES, filesSaga);
}

export function* watchGetFilesFromStorage(){

  yield takeLatest(types.GET_FILES_FROM_STORAGE, storageSaga);
}

